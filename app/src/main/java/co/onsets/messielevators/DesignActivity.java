package co.onsets.messielevators;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DesignActivity extends AppCompatActivity{

    private TextView mTextMessage;
    private EditText etMotorCapacity, etOverhead, etShaftWidth, etShaftDepth, etCabinWidth,
            etCabinDepth, etDoorWidth, etDoorHeight, etPassenger, etCapacity;
    Spinner typeSpinner,passengerSpinner, capacitySpinner, speedSpinner;
    LinearLayout llShaftSize;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_design:
                    mTextMessage.setText(R.string.title_design);
                    return true;
                case R.id.navigation_drawing:
                    mTextMessage.setText(R.string.title_drawing);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design);

        etCapacity = findViewById(R.id.etCapacity);
        etPassenger = findViewById(R.id.etPassenger);
        etCabinDepth = findViewById(R.id.etCabinDepth);
        etCabinWidth = findViewById(R.id.etCabinWidth);
        etDoorHeight = findViewById(R.id.etDoorHeight);
        etDoorWidth = findViewById(R.id.etDoorWidth);
        etShaftDepth = findViewById(R.id.etShaftDepth);
        etShaftWidth = findViewById(R.id.etShaftWidth);
        etMotorCapacity = findViewById(R.id.etMotorCapacity);
        etOverhead = findViewById(R.id.etOverHead);
        llShaftSize = findViewById(R.id.llShaftSize);

        typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.types_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        typeSpinner.setAdapter(adapter);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){

                }
                else if(position == 1){
                    etShaftDepth.setEnabled(false);
                    etShaftWidth.setEnabled(false);
                    etPassenger.setEnabled(false);
                    passengerSpinner.setVisibility(View.GONE);
                    etPassenger.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        speedSpinner = (Spinner) findViewById(R.id.speed_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.speed_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        speedSpinner.setAdapter(adapter3);

        passengerSpinner = (Spinner) findViewById(R.id.passenger_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.passenger_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        passengerSpinner.setAdapter(adapter1);

        capacitySpinner = (Spinner) findViewById(R.id.capacity_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.capacity_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        capacitySpinner.setAdapter(adapter2);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
